## Autonomous Vehicule - CAN Bus - Practical work

### TP n°1

##TESTS

#Test n°1 :

**Command** : TERM 1 : candump vcan0
	      TERM 2 : cansend vcan0 123#F0A1DD03

**RECEPTION** : TERM 1 : vcan0  123   [4]  F0 A1 DD 03



#Test n°2 :

**Command** : TERM 1 : candump vcan0
	      TERM 2 : cansend vcan0 123#E7B4A3FF

**RECEPTION** : TERM 1 : vcan0  123   [4]  E7 B4 A3 FF


##Test.c

Réalisation du code à partir des fichiers canreceive, canfilter et cantrasmit

Changement des valeurs : 

**frame.can_id** = 0x8123
**frame.can_dlc** = 8
**rfilter[0].can_id** = 0x100
**rfilter[0].can_mask** = 0xF00

On lance cangen vcan0 dans un premier terminal, candump vcan0 dans un deuxième puis ./test dans un troisième qui va filtrer et recevoir une ligne (exemple) : vcan0  279   [8]  BD E0 46 54 F7 B2 EF 13

Elle est composée du nom, de l'ID, du nombre de bytes et de ces bytes.

##Vehicle_checker

![Interface](/TP/SAT/interface.png "Interface du simulateur")



On lance dans un premier terminal le simulateur : __python -m avsim2D__

Dans un dernier terminal, on lance notre fichier test : __candump vcan0__

Dans un deuxième terminal, on lance vehicle_checker : ./vehicle_checker

On observe en suite les frames reçues en fonction des différentes actions (attention à bien repérer l'ID 123).


###RECEPTION DE FRAMES

**Clignotant droit** : 0x123 [2] 00 01

**Clignotant gauche** : 0x123 [2] 00 02

**Phare faible lumière on/off** : 0x123 [2] 01 00

**Phare forte lumière on/off** : 0x123 [2] 02 00

**Extinction des feux avant démarrage** : 0x123 [2] 00 00




